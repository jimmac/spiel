/* editablePicture.js
 *
 * Copyright 2022 Georges Basile Stavracas Neto <georges.stavracas@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

import Gdk from 'gi://Gdk';
import Gio from 'gi://Gio';
import GLib from 'gi://GLib';
import GObject from 'gi://GObject';
import Gtk from 'gi://Gtk';

export const SpielEditablePicture = GObject.registerClass({
    GTypeName: 'SpielEditablePicture',
    Template: 'resource:///com/feaneron/Spiel/editablePicture.ui',
    CssName: 'editablepicture',
    Properties: {
        'can-shrink': GObject.ParamSpec.boolean('can-shrink', '', '',
            GObject.ParamFlags.READWRITE,
            false),
        'content-fit': GObject.ParamSpec.enum('content-fit', '', '',
            GObject.ParamFlags.READWRITE,
            Gtk.ContentFit.$gtype,
            Gtk.ContentFit.FILL),
        'editable': GObject.ParamSpec.boolean('editable', '', '',
            GObject.ParamFlags.READWRITE,
            true),
        'file': GObject.ParamSpec.object('file', '', '',
            GObject.ParamFlags.READWRITE,
            Gio.File.$gtype),
        'icon-size': GObject.ParamSpec.int('icon-size', '', '',
            GObject.ParamFlags.READWRITE,
            -1, GLib.MAXINT32, -1),
        'paintable': GObject.ParamSpec.object('paintable', '', '',
            GObject.ParamFlags.READWRITE,
            Gdk.Paintable.$gtype),
    },
    InternalChildren: ['overlay'],
}, class SpielEditablePicture extends Gtk.Widget {
    _init(params = {}) {
        super._init();

        this._editable = true;
        this._iconSize = -1;

        this.add_css_class('editable');
    }

    vfunc_get_request_mode() {
        return Gtk.SizeRequestMode.HEIGHT_FOR_WIDTH;
    }

    _onSelectPictureButtonClickedCb() {
        const filechooser = new Gtk.FileChooserNative({
           modal: true,
           transient_for: this.get_root(),
        });
        filechooser.connect('response', (dialog, response) => {
            if (response == Gtk.ResponseType.ACCEPT)
                this.file = filechooser.get_file();
        });

        const filter = new Gtk.FileFilter();
        filter.set_name(_('Images'));
        filter.add_mime_type('image/*');
        filechooser.add_filter(filter);

        filechooser.show();
    }

    vfunc_measure(orientation, forSize) {
        const {iconSize} = this;
        if (iconSize !== -1)
            return [iconSize, iconSize, -1, -1];
        return this._overlay.measure(orientation, forSize);
    }

    vfunc_size_allocate(width, height, baseline) {
        this._overlay.allocate(width, height, baseline, null);
    }

    get editable() {
        return this._editable;
    }

    set editable(v) {
        if (this._editable === v)
            return;

        this._editable = v;

        if (this._editable)
            this.add_css_class('editable');
        else
            this.remove_css_class('editable');

        this.notify('editable');
    }

    get icon_size() {
        return this._iconSize;
    }

    set icon_size(size) {
        if (this._size === size)
            return;

        this._iconSize = size;
        this.queue_resize();
        this.notify('icon-size');
    }
});
