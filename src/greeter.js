/* window.js
 *
 * Copyright 2022 Georges Stavracas
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

import Adw from 'gi://Adw';
import GLib from 'gi://GLib';
import Gio from 'gi://Gio';
import GObject from 'gi://GObject';
import Gtk from 'gi://Gtk';

import { SpielSentinelItem, SpielSentinelListModel } from './sentinelModel.js'
import { SpielTalk } from './talkShop.js';
import { SpielTalkThumbnail } from './thumbnail.js';
import { SpielWindow } from './window.js';

const SpielTalkItem = GObject.registerClass({
    GTypeName: 'SpielTalkItem',
    Properties: {
        'talk': GObject.ParamSpec.object('talk', '', '',
            GObject.ParamFlags.READWRITE | GObject.ParamFlags.CONSTRUCT_ONLY,
            SpielTalk.$gtype),
    },
}, class SpielTalkItem extends Gtk.FlowBoxChild {
    constructor(talk) {
        super({talk});

        const box = new Gtk.Box({
            orientation: Gtk.Orientation.VERTICAL,
            spacing: 6,
            margin_top: 12,
            margin_bottom: 12,
            margin_start: 12,
            margin_end: 12,
        });

        const preview = new SpielTalkThumbnail({talk});
        box.append(preview);

        const label = new Gtk.Label();
        this.talk.bind_property('name', label, 'label', GObject.BindingFlags.SYNC_CREATE);
        box.append(label);

        this.child = new Gtk.Button({
            child: box,
            css_classes: ['flat'],
            width_request: 207,
            height_request: 128,
        });

        this.child.connect('clicked', () => this.emit('activate'));
    }
});


const SpielNewTalkItem = GObject.registerClass({
    GTypeName: 'SpielNewTalkItem',
}, class SpielNewTalkItem extends Gtk.FlowBoxChild {
    constructor() {
        super();

        this.child = new Gtk.Button({
            child: new Gtk.Image({
                vexpand: true,
                icon_name: 'list-add-symbolic',
                pixel_size: 32,
            }),
            css_classes: ['flat'],
            width_request: 207,
            height_request: 128,
        });

        this.child.connect('clicked', () => this.emit('activate'));
    }
});

export const SpielGreeter = GObject.registerClass({
    GTypeName: 'SpielGreeter',
    Template: 'resource:///com/feaneron/Spiel/greeter.ui',
    InternalChildren: [
        'backButton',
        'headerbar',
        'newTalkNameRow',
        'searchEntry',
        'stack',
        'talksFlowbox',
    ],
}, class SpielGreeter extends Adw.ApplicationWindow {
    _init(application) {
        super._init({ application });

        this.add_action_entries([
            {
                name: 'create-talk',
                activate: () => this._createTalk(),
            },
        ]);

        this._application = application;

        const {talks} = this._application.talkShop;
        const sortedTalks = new Gtk.SortListModel({
            model: new Gtk.FilterListModel({
                model: new SpielSentinelListModel({model: talks}),
                filter: Gtk.CustomFilter.new(item => {
                    const {text} = this._searchEntry;
                    if (text === '')
                        return true;
                    if (item instanceof SpielSentinelItem)
                        return false;
                    return item.name.toLowerCase().indexOf(text.toLowerCase()) !== -1;
                }),
            }),
            sorter: Gtk.CustomSorter.new((a, b) => {
                if (a instanceof SpielSentinelItem)
                    return 1;
                else if (b instanceof SpielSentinelItem)
                    return -1;
                else
                    return b.lastOpened - a.lastOpened;
            }),
        });

        this._searchEntry.connect('notify::text',
            () => sortedTalks.model.filter.changed(Gtk.FilterChange.DIFFERENT));

        this._talksFlowbox.bind_model(
            sortedTalks,
            item => {
                if (item instanceof SpielSentinelItem)
                    return new SpielNewTalkItem();
                else
                    return new SpielTalkItem(item);
            });

        this.lookup_action('create-talk').enabled = false;

        this._stack.visibleChildName = talks.nItems > 0 ? 'greeter' : 'empty';
    }

    _openTalk(talk) {
        let window;

        // If there's an open window with that talk, raise it
        for (const win of this._application.get_windows()) {
            if ((win instanceof SpielWindow) && win.talk.id === talk.id) {
                window = win;
                break;
            }
        }

        if (!window)
            window = new SpielWindow(this._application, talk);

        window.present();

        this.close();
    }

    _createTalk() {
        const {talkShop} = this._application;

        const name = this._newTalkNameRow.text.trim();
        const newTalk = talkShop.createTalk(name);

        this._openTalk(newTalk);
    }

    _goToNewTalkPage() {
        this._newTalkNameRow.text = '';
        this._stack.visibleChildName = 'new-talk';
    }

    _onBackButtonClickedCb() {
        const {talks} = this._application.talkShop;
        this._stack.visibleChildName = talks.nItems > 0 ? 'greeter' : 'empty';
    }

    _onCreateTalkButtonClickedCb() {
        this._goToNewTalkPage();
    }

    _onNewTalkNameRowTextChangedCb() {
        const action = this.lookup_action('create-talk');
        action.enabled = this._newTalkNameRow.text.trim().length > 0;
    }

    _onStackVisibleChildChangedCb() {
        const {name, title} = this._stack.get_page(this._stack.visibleChild);

        this.title = _('Spiel — %s').format(title);
        this._headerbar.titleWidget.title = title;

        this._backButton.visible = name !== 'greeter';
    }

    _onTalksFlowboxChildActivatedCb(flowbox, child) {
        if (child instanceof SpielNewTalkItem)
            this._goToNewTalkPage();
        else if (child instanceof SpielTalkItem)
            this._openTalk(child.talk);
    }
});
