/* slide.js
 *
 * Copyright 2022 Georges Basile Stavracas Neto <georges.stavracas@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

import Adw from 'gi://Adw';
import GLib from 'gi://GLib';
import Gio from 'gi://Gio';
import GObject from 'gi://GObject';
import Gtk from 'gi://Gtk';

import {SpielEditablePicture} from './editablePicture.js';

export const Slide = GObject.registerClass({
    GTypeName: 'SpielSlide',
    CssName: 'slide',
    Properties: {
        'background': GObject.ParamSpec.string('background', '', '',
            GObject.ParamFlags.READWRITE,
            null),
        'dark': GObject.ParamSpec.boolean('dark', '', '',
            GObject.ParamFlags.READWRITE,
            true),
        'editable': GObject.ParamSpec.boolean('editable', '', '',
            GObject.ParamFlags.READWRITE,
            true),
        'title': GObject.ParamSpec.string('title', '', '',
            GObject.ParamFlags.READWRITE,
            'Slide Title'),
        'type': GObject.ParamSpec.string('type', '', '',
            GObject.ParamFlags.READWRITE | GObject.ParamFlags.CONSTRUCT_ONLY,
            null),
    },
}, class Slide extends Adw.Bin {
    _init(params = {}) {
        super._init({
            hexpand: true,
            vexpand: true,
            ...params,
        });
    }

    get background() {
        return this._background;
    }

    set background(v) {
        const context = this.get_style_context();

        if (this._cssProvider && !v) {
            context.add_provider(this._cssProvider);
            delete this._cssProvider;
        }

        this._background = v;

        if (v) {
            const cssName = this.constructor.get_css_name();

            if (!this._cssProvider) {
                this._cssProvider = new Gtk.CssProvider();
                context.add_provider(
                    this._cssProvider,
                    Gtk.StyleProvider.PRIORITY_APPLICATION);
            }

            this._cssProvider.load_from_data(`${cssName} {background: ${v};}`);
        }

    }

    toJSON() {
        return {
            'type': this.type,
            'title': this.title ? this.title : '',
            'dark': this.dark,
            'background': this.background ? this.background : '',
        };
    }
});

export const BulletPointSlide = GObject.registerClass({
    GTypeName: 'SpielBulletPointSlide',
    Template: 'resource:///com/feaneron/Spiel/bulletPointSlide.ui',
    CssName: 'bulletpointslide',
    Properties: {
        'bullet-points': GObject.ParamSpec.boxed('bullet-points', '', '',
            GObject.ParamFlags.READWRITE,
            GLib.strv_get_type()),
    },
    InternalChildren: ['bodyText'],
}, class OpeningSlide extends Slide {
    _init(params = {}) {
        Slide.$gtype;

        super._init({type: 'bullet-points', ...params});

        this._bodyText.remove_css_class('view');
        this.bind_property_full('bullet-points',
            this._bodyText.buffer, 'text',
            GObject.BindingFlags.SYNC_CREATE | GObject.BindingFlags.BIDIRECTIONAL,
            (bind, source) => [true, ` • ${source ? source.join('\n • ') : ''}`],
            (bind, source) => {
                const lines = source ? source.split('\n') : [];
                const trimmed = lines.map(line => {
                    if (line.startsWith(' • '))
                        return line.slice(' • '.length).trim();
                    return line.trim();
                });
                return [true, trimmed];
            });
    }

    toJSON() {
        return {
            ...super.toJSON(),
            'bullet-points': this.bulletPoints,
        };
    }
});

export const IconGridSlide = GObject.registerClass({
    GTypeName: 'SpielIconGridSlide',
    Template: 'resource:///com/feaneron/Spiel/iconGridSlide.ui',
    CssName: 'bulletpointslide',
    Properties: {
        'icon-size': GObject.ParamSpec.uint('icon-size', '', '',
            GObject.ParamFlags.READWRITE,
            1, GLib.MAXUINT32, 64),
        'icons': GObject.ParamSpec.boxed('icons', '', '',
            GObject.ParamFlags.READWRITE,
            GLib.strv_get_type()),
    },
    InternalChildren: ['grid'],
}, class IconGridSlide extends Slide {
    _init(params = {}) {
        Slide.$gtype;

        super._init({type: 'icon-grid', ...params});

        const {icons} = this;
        const nColumns = this.icons.length > 3 ? Math.ceil(Math.sqrt(icons.length)) : icons.length;
        for (let i = 0; i < icons.length; i++) {
            const image = new Gtk.Image({
                cssClasses: ['icon-dropshadow'],
                pixelSize: this.iconSize,
            });

            if (icons[i].startsWith('resource://'))
                image.resource = icons[i].slice('resource://'.length);
            else
                image.file = icons[i];

            this._grid.attach(image, i % nColumns, Math.floor(i / nColumns), 1, 1);
        }
    }

    toJSON() {
        return {
            ...super.toJSON(),
            'icon-size': this.iconSize,
            'icons': this.icons,
        };
    }
});

export const TitleSlide = GObject.registerClass({
    GTypeName: 'SpielTitleSlide',
    Template: 'resource:///com/feaneron/Spiel/titleSlide.ui',
    CssName: 'titleslide',
}, class TitleSlide extends Slide {
    _init(params = {}) {
        Slide.$gtype;

        super._init({type: 'title', ...params});
    }
});

export const OpeningSlide = GObject.registerClass({
    GTypeName: 'SpielOpeningSlide',
    Properties: {
        'presenter': GObject.ParamSpec.string('presenter', '', '',
            GObject.ParamFlags.READWRITE,
            'Presenter name'),
    },
    Template: 'resource:///com/feaneron/Spiel/openingSlide.ui',
    CssName: 'openingslide',
}, class OpeningSlide extends Slide {
    _init(params = {}) {
        Slide.$gtype;

        super._init({type: 'opening', ...params});
    }

    toJSON() {
        return {
            ...super.toJSON(),
            'presenter': this.presenter,
        };
    }
});

export const PictureSlide = GObject.registerClass({
    GTypeName: 'SpielPictureSlide',
    Template: 'resource:///com/feaneron/Spiel/pictureSlide.ui',
    CssName: 'pictureslide',
    Properties: {
        'file': GObject.ParamSpec.object('file', '', '',
            GObject.ParamFlags.READWRITE,
            Gio.File.$gtype),
    },
    InternalChildren: ['picture'],
}, class PictureSlide extends Slide {
    _init(params = {}) {
        SpielEditablePicture.$gtype;
        Slide.$gtype;

        if (params['file'])
            params['file'] = Gio.File.new_for_uri(params['file']);

        super._init({type: 'picture', ...params});
    }

    toJSON() {
        return {
            ...super.toJSON(),
            'file': this.file?.get_uri(),
        };
    }
});

export function createSlide(slide) {
    switch (slide.type) {
        case 'bullet-points':
            return new BulletPointSlide(slide);
        case 'icon-grid':
            return new IconGridSlide(slide);
        case 'opening':
            return new OpeningSlide(slide);
        case 'picture':
            return new PictureSlide(slide);
        case 'title':
            return new TitleSlide(slide);
        default:
            throw new Error(`Unknown slide type ${type}`);
    }
}

export function listAvailableSlides() {
    return [
        {
            type: 'bullet-points',
            name: _('Bullet Points'),
            description: _('A slide with a title, and bullet points'),
            constructor: BulletPointSlide,
        },
        {
            type: 'icon-grid',
            name: _('Icons'),
            description: _('A grid of icons'),
            constructor: IconGridSlide,
        },
        {
            type: 'opening',
            name: _('Opening'),
            description: _('Opening with title and author'),
            constructor: OpeningSlide,
        },
        {
            type: 'picture',
            name: _('Picture'),
            description: _('A simple picture covering the entire slide'),
            constructor: PictureSlide,
        },
        {
            type: 'title',
            name: _('Title'),
            description: _('Simple slide with a centered title'),
            constructor: PictureSlide,
        },
    ];
}
