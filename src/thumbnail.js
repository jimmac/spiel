/* thumbnail.js
 *
 * Copyright 2022 Georges Basile Stavracas Neto <georges.stavracas@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

import Adw from 'gi://Adw';
import GLib from 'gi://GLib';
import GObject from 'gi://GObject';
import Graphene from 'gi://Graphene';
import Gsk from 'gi://Gsk';
import Gtk from 'gi://Gtk';

import {createSlide} from './slides.js';
import {SpielTalk} from './talkShop.js';

export const SpielSlideThumbnail = GObject.registerClass({
    ClassName: 'SpielSlideThumbnail',
    CssName: 'slidethumbnail',
    Properties: {
        'size': GObject.ParamSpec.uint64('size', '', '',
            GObject.ParamFlags.READWRITE,
            0, GLib.MAXUINT64_BIGINT, 128),
        'slide': GObject.ParamSpec.object('slide', '', '',
            GObject.ParamFlags.READWRITE | GObject.ParamFlags.CONSTRUCT_ONLY,
            GObject.Object.$gtype),
    },
}, class SpielSlideThumbnail extends Gtk.Widget {

    constructor(params) {
        super(params);

        this.slide.connect('notify', () => this._update());
        this._update();
    }

    _update() {
        if (this._clone)
            this._clone.unparent();

        this._clone = createSlide(this.slide.toJSON());
        this._clone.editable = false;
        this._clone.can_target = false;
        this._clone.set_parent(this);
    }

    vfunc_get_request_mode() {
        return Gtk.SizeRequestMode.HEIGHT_FOR_WIDTH;
    }

    vfunc_measure(orientation, forSize) {
        const ratio = 1920 / 1080;

        switch (orientation) {
        case Gtk.Orientation.HORIZONTAL:
            return [this.size, this.size, -1, -1];
        case Gtk.Orientation.VERTICAL:
            return [forSize / ratio, forSize / ratio, -1, -1];
        }
    }

    vfunc_size_allocate(width, height, baseline) {
        const hScale = width / 1920;
        const vScale = height / 1080;
        const scale = Math.min(hScale, vScale);

        const offset = {
            x: (width - 1920 * scale) / 2,
            y: (height - 1080 * scale) / 2,
        };

        let transform = new Gsk.Transform();
        transform = transform.translate(new Graphene.Point(offset));
        transform = transform.scale(scale, scale);

        this._clone.allocate(1920, 1080, baseline, transform);
    }

});


export const SpielTalkThumbnail = GObject.registerClass({
    ClassName: 'SpielTalkThumbnail',
    CssName: 'talkthumbnail',
    Properties: {
        'size': GObject.ParamSpec.uint64('size', '', '',
            GObject.ParamFlags.READWRITE,
            0, GLib.MAXUINT64_BIGINT, 128),
        'talk': GObject.ParamSpec.object('talk', '', '',
            GObject.ParamFlags.READWRITE | GObject.ParamFlags.CONSTRUCT_ONLY,
            SpielTalk.$gtype),
    },
}, class SpielTalkThumbnail extends Adw.Bin {

    constructor(params) {
        super(params);

        this.talk.slideDeck.connect('items-changed', () => this._updateSlideThumbnail());
        this._updateSlideThumbnail();
    }

    _updateSlideThumbnail() {
        const firstSlide = this.talk.slideDeck.get_item(0);
        if (firstSlide) {
            this.child = new SpielSlideThumbnail({
                slide: firstSlide,
                size: this.size,
            });
            this.bind_property('size', this.child, 'size', GObject.BindingFlags.SYNC_CREATE);
        } else {
            this.child = null;
        }
    }

});

