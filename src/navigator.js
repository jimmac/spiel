/* navigator.js
 *
 * Copyright 2022 Georges Basile Stavracas Neto <georges.stavracas@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */


import Adw from 'gi://Adw';
import GLib from 'gi://GLib';
import Gdk from 'gi://Gdk';
import Gio from 'gi://Gio';
import GObject from 'gi://GObject';
import Gtk from 'gi://Gtk';

import { SpielSentinelItem, SpielSentinelListModel } from './sentinelModel.js'
import { SpielSlideThumbnail } from './thumbnail.js';
import { SpielTalk } from './talkShop.js';

const SpielNavigatorItem = GObject.registerClass({
    GTypeName: 'SpielNavigatorItem',
    Template: 'resource:///com/feaneron/Spiel/navigatorItem.ui',
    CssName: 'navigatoritem',
    Properties: {
        'item': GObject.ParamSpec.object('item', '', '',
            GObject.ParamFlags.READWRITE,
            GObject.Object.$gtype),
        'position': GObject.ParamSpec.uint64('position', '', '',
            GObject.ParamFlags.READWRITE,
            0, GLib.MAXUINT64_BIGINT, 0),
    },
    InternalChildren: [
        'contextMenu',
        'newItem',
        'preview',
        'stack',
    ],
}, class SpielNavigatorItem extends Gtk.Widget {
    constructor() {
        super();

        this._item = null;

        this._actionGroup = new Gio.SimpleActionGroup();
        this._actionGroup.add_action_entries([
            {
                name: 'add-after',
                activate: () => this._addSlide(this.position + 1),
            },
            {
                name: 'add-before',
                activate: () => this._addSlide(this.position),
            },
            {
                name: 'remove',
                activate: () => this._remove(),
            },
        ]);
        this.insert_action_group('navigatoritem', this._actionGroup);
    }

    _onClickGesturePressedCb(clickGesture, nPress, x, y) {
        [, x, y] = clickGesture.widget.translate_coordinates(this, x, y);

        this._contextMenu.set_pointing_to(new Gdk.Rectangle({x, y}));
        this._contextMenu.popup();
    }

    _addSlide(position) {
        const positionVariant = new GLib.Variant('i', position);
        this.activate_action('win.add-slide', positionVariant);
    }

    _remove() {
        const positionVariant = new GLib.Variant('u', this.position);
        this.activate_action('win.remove-slide', positionVariant);
    }

    get item() {
        return this._item;
    }

    set item(v) {
        if (this._item === v)
            return;

        this._item = v;

        if (v instanceof SpielSentinelItem) {
            this._stack.visibleChild = this._newItem;
            this._preview.child = null;
        } else if (v) {
            this._stack.visibleChild = this._preview;
            this._preview.child = new SpielSlideThumbnail({
                size: 96,
                slide: v,
            });
        }

        this.notify('item');
    }
});

export const SpielNavigator = GObject.registerClass({
    GTypeName: 'SpielNavigator',
    Template: 'resource:///com/feaneron/Spiel/navigator.ui',
    CssName: 'navigator',
    Properties: {
        'current-slide': GObject.ParamSpec.double('current-slide', '', '',
            GObject.ParamFlags.READWRITE,
            0, 100000, 0),
        'talk': GObject.ParamSpec.object('talk', '', '',
            GObject.ParamFlags.READWRITE,
            SpielTalk.$gtype),
    },
    InternalChildren: ['selectionModel'],
}, class SpielNavigator extends Adw.Bin {
    constructor() {
        SpielNavigatorItem.$gtype;

        super();

        this._talk = null;
        this._model = new SpielSentinelListModel();
        this._selectionModel.model = this._model;

        this.bind_property_full('current-slide',
            this._selectionModel, 'selected',
            GObject.BindingFlags.SYNC_CREATE | GObject.BindingFlags.BIDIRECTIONAL,
            (bind, source) => [true, Math.round(source)],
            (bind, source) => [true, Math.round(source)]);
    }

    _setupListItem(factory, listItem) {
        const navigatorItem = new SpielNavigatorItem();
        listItem.activatable = false;
        listItem.selectable = false;
        listItem.child = navigatorItem;
        listItem.bind_property('item', navigatorItem, 'item', GObject.BindingFlags.DEFAULT);
        listItem.bind_property('position', navigatorItem, 'position', GObject.BindingFlags.DEFAULT);
    }

    _bindListItem(factory, listItem) {
        listItem.selectable = !(listItem.item instanceof SpielSentinelItem);
    }

    get talk() {
        return this._talk;
    }

    set talk(v) {
        if (this._talk === v)
            return;

        this._talk = v;
        this._model.model = this._talk?.slideDeck;
        this.notify('talk');
    }
});
