/* window.js
 *
 * Copyright 2022 Georges Stavracas
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

import Adw from 'gi://Adw';
import GLib from 'gi://GLib';
import Gio from 'gi://Gio';
import GObject from 'gi://GObject';
import Gtk from 'gi://Gtk';

import {createSlide} from './slides.js';
import {SpielGreeter} from './greeter.js';
import {SpielNavigator} from './navigator.js';
import {SpielNewSlideDialog} from './newSlide.js';
import {SpielTalk} from './talkShop.js';

export const SpielWindow = GObject.registerClass({
    GTypeName: 'SpielWindow',
    Template: 'resource:///com/feaneron/Spiel/window.ui',
    Properties: {
        'current-slide': GObject.ParamSpec.double('current-slide', '', '',
            GObject.ParamFlags.READWRITE,
            0, 100000, 0),
        'editable': GObject.ParamSpec.boolean('editable', '', '',
            GObject.ParamFlags.READWRITE,
            true),
        'talk': GObject.ParamSpec.object('talk', '', '',
            GObject.ParamFlags.READWRITE | GObject.ParamFlags.CONSTRUCT_ONLY,
            SpielTalk.$gtype),
    },
    InternalChildren: [
        'adwaitaPicture',
        'carousel',
        'fullscreenButton',
        'headerbar',
        'headerbarMotionController',
        'headerbarRevealer',
        'mainBox',
        'navigator',
        'title',
    ],
}, class SpielWindow extends Adw.ApplicationWindow {
    constructor(application, talk) {
        SpielNavigator.$gtype;

        const [maximized, defaultWidth, defaultHeight] =
            application.settings.get_value('window-state').deep_unpack();

        super({
            application,
            talk,
            maximized,
            defaultWidth,
            defaultHeight,
        });

        this._application = application;

        this.add_action_entries([
            {
                name: 'add-slide',
                parameter_type: 'i',
                activate: (action, param) => this._addSlide(param.deep_unpack()),
            },
            {
                name: 'first-slide',
                activate: () => this._scrollToPage(0),
            },
            {
                name: 'last-slide',
                activate: () => this._scrollToPage(this._carousel.nPages - 1),
            },
            {
                name: 'next-slide',
                activate: () => this._goToNext(),
            },
            {
                name: 'open-talk',
                activate: () => this._openTalk(),
            },
            {
                name: 'previous-slide',
                activate: () => this._goToPrevious(),
            },
            {
                name: 'remove-slide',
                parameter_type: 'u',
                activate: (action, param) => this._removeSlide(param.deep_unpack()),
            },
            {
                name: 'toggle-fullscreen',
                state: 'false',
                change_state: () => this._toggleFullscreen(),
            },
            {
                name: 'unfullscreen',
                activate: () => {
                    if (this.fullscreened)
                        this._toggleFullscreen()
                },
            },
        ]);

        this.talk.slideDeck.connect(
            'items-changed',
            (model, position, removed, added) => {
                while (removed-- > 0) {
                    const nthPage = this._carousel.get_nth_page(position);
                    this._carousel.remove(nthPage);
                }

                while (added-- > 0) {
                    const newSlide = model.get_item(position);
                    this.bind_property('editable', newSlide, 'editable', GObject.BindingFlags.SYNC_CREATE);
                    this._carousel.insert(newSlide, position);
                    position++;
                }
            });

        for (const slide of talk.slideDeck) {
            this.bind_property('editable', slide, 'editable', GObject.BindingFlags.SYNC_CREATE);
            this._carousel.append(slide);
        }

        this._navigator.talk = talk;
        talk.bind_property('name', this._title, 'title', GObject.BindingFlags.SYNC_CREATE);
        talk.bind_property_full('name',
            this, 'title',
            GObject.BindingFlags.SYNC_CREATE,
            (source, value) => [true, _('Spiel — %s').format(value)],
            null);
        this._scrollToPage(0);

        this._carousel.connect('notify::position', () => {
            this._updatingCarousel = true;
            this.currentSlide = this._carousel.position;
            this._updatingCarousel = false;
        });

        this.connect('notify::current-slide', () => {
            if (this._updatingCarousel)
                return;

            this._scrollToPage(Math.round(this.currentSlide));
        });

        const now = GLib.DateTime.new_now_local();
        talk.lastOpened = now.to_unix();
    }

    _addSlide(position = -1) {
        const {nItems} = this.talk.slideDeck;

        position = position === -1 ? nItems : Math.min(position, nItems);

        const dialog = new SpielNewSlideDialog({
            transient_for: this,
        });
        dialog.connect('create-slide', (dialog, type) => {
            const newSlide = createSlide({type});
            this.talk.slideDeck.insert(position, newSlide);

            // We can't simply call _scrollToPage() because the carousel
            // may not have a valid allocation at this point
            GLib.idle_add(GLib.PRIORITY_LOW, () => {
                this._scrollToPage(position);
                return GLib.SOURCE_REMOVE;
            })
        });
        dialog.present();
    }

    _openTalk() {
        let window;

        // If there's an open window with that talk, raise it
        for (const win of this._application.get_windows()) {
            if (win instanceof SpielGreeter) {
                window = win;
                break;
            }
        }

        if (!window)
            window = new SpielGreeter(this._application);

        window.present();
    }

    _removeSlide(position) {
        if (position >= this.talk.slideDeck.nItems)
            throw new Error('Cannot remove slide at invalid position');

        this.talk.slideDeck.remove(position);
    }

    _scrollToPage(page) {
        if (page < 0 || page > this._carousel.nPages - 1)
            return;

        const widget = this._carousel.get_nth_page(page);
        this._carousel.scroll_to(widget, true);

        const {dark} = this.talk.slideDeck.get_item(page);
        this._adwaitaPicture.file = dark
            ? Gio.File.new_for_uri(`resource:///com/feaneron/Spiel/assets/adwaita-light.png`)
            : Gio.File.new_for_uri(`resource:///com/feaneron/Spiel/assets/adwaita-dark.png`);
        this.currentSlide = page;
    }

    _goToNext() {
        const {position, nPages} = this._carousel;
        const nextPage = Math.min(Math.floor(position) + 1, nPages - 1);
        this._scrollToPage(nextPage);
    }

    _goToPrevious() {
        const {position, nPages} = this._carousel;
        const previousPage = Math.max(Math.floor(position) - 1, 0);
        this._scrollToPage(previousPage);
    }

    _maybeHideHeaderBar() {
        const {containsPointer} = this._headerbarMotionController;

        if (!containsPointer)
            this._headerbarRevealer.revealChild = false;

        return !containsPointer;
    }

    _clearHeaderBarRevealTimer() {
        if (this._revealHeaderBarId) {
            GLib.source_remove(this._revealHeaderBarId);
            delete this._revealHeaderBarId;
        }
    }

    _getScopeFromEditableStateCb(controller, editable) {
        return editable ? Gtk.PropagationPhase.NONE : Gtk.PropagationPhase.CAPTURE;
    }

    _onHeaderbarMotionControllerEnterCb(motionController, x, y) {
        this._clearHeaderBarRevealTimer();
        this._headerbarRevealer.revealChild = true;
    }

    _onHeaderbarMotionControllerLeaveCb(motionController) {
        this._clearHeaderBarRevealTimer();

        this._revealHeaderBarId = GLib.timeout_add(GLib.PRIORITY_LOW, 1500, () => {
            if (!this._maybeHideHeaderBar())
                return GLib.SOURCE_CONTINUE;

            delete this._revealHeaderBarId;
            return GLib.SOURCE_REMOVE;
        });
    }

    _revealHeaderBar() {
        if (this._headerbarRevealer.revealChild)
            return;

        this._headerbarRevealer.revealChild = true;
        this._revealHeaderBarId = GLib.timeout_add(GLib.PRIORITY_LOW, 1500, () => {
            if (!this._maybeHideHeaderBar())
                return GLib.SOURCE_CONTINUE;

            delete this._revealHeaderBarId;
            return GLib.SOURCE_REMOVE;
        });
    }

    _toggleFullscreen() {
        this._headerbarMotionController.propagationPhase = this.fullscreened
            ? Gtk.PropagationPhase.NONE
            : Gtk.PropagationPhase.BUBBLE;

        if (!this.fullscreened) {
            this.editable = false;
            this.fullscreen();
            this._headerbarRevealer.revealChild = false;
            this._headerbarRevealer.visible = true;
            this._mainBox.remove(this._headerbar);
            this._headerbarRevealer.set_child(this._headerbar);
            this._fullscreenButton.active = true;
            this._carousel.grab_focus();
            this._revealHeaderBar();
        } else {
            this.editable = true;
            this.unfullscreen();
            this._headerbarRevealer.set_child(null);
            this._mainBox.prepend(this._headerbar);
            this._fullscreenButton.active = false;
            this._headerbarRevealer.revealChild = false;
            this._headerbarRevealer.visible = false;
        }
    }

    vfunc_close_request() {
        this._clearHeaderBarRevealTimer();

        // Save window state
        const lastState =
            this._application.settings.get_value('window-state').deep_unpack();
        const state = [
            this.maximized,
            this.maximized ? lastState[1] : this.get_width(),
            this.maximized ? lastState[2] : this.get_height(),
        ];
        this._application.settings.set_value('window-state', new GLib.Variant('(bii)', state));

        this._application.talkShop.saveTalk(this.talk);

        for (const slide of this.talk.slideDeck)
            this._carousel.remove(slide);

        return super.vfunc_close_request();
    }
});

