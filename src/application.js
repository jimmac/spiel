/* application.js
 *
 * Copyright 2022 Georges Stavracas
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

import GObject from 'gi://GObject';
import Gio from 'gi://Gio';
import Gtk from 'gi://Gtk?version=4.0';
import Adw from 'gi://Adw?version=1';

import { SpielGreeter } from './greeter.js';
import { SpielTalkShop } from './talkShop.js';

pkg.initGettext();
pkg.initFormat();

export const SpielApplication = GObject.registerClass({
    GTypeName: 'SpielApplication',
    Properties: {
        'settings': GObject.ParamSpec.object('settings', '', '',
            GObject.ParamFlags.READABLE,
            Gio.Settings.$gtype),
        'talk-shop': GObject.ParamSpec.object('talk-shop', '', '',
            GObject.ParamFlags.READABLE,
            SpielTalkShop.$gtype),
    },
}, class SpielApplication extends Adw.Application {
    constructor() {
        super({
          application_id: 'com.feaneron.Spiel',
          flags: Gio.ApplicationFlags.FLAGS_NONE,
        });

        const quit_action = new Gio.SimpleAction({name: 'quit'});
            quit_action.connect('activate', action => {
            this.quit();
        });
        this.add_action(quit_action);
        this.set_accels_for_action('app.quit', ['<primary>q']);

        const show_about_action = new Gio.SimpleAction({name: 'about'});
        show_about_action.connect('activate', action => {
            const aboutWindow = new Adw.AboutWindow({
                transient_for: this.active_window,
                application_name: 'Spiel',
                application_icon: 'com.feaneron.Spiel',
                developer_name: 'Georges Basile Stavracas Neto',
                version: '0.1.0',
                developers: [
                    'Georges Basile Stavracas Neto <georges.stavracas@gmail.com>'
                ],
                copyright: '© 2022 Georges Stavracas'
            });
            aboutWindow.present();
        });
        this.add_action(show_about_action);

        this._talkShop = new SpielTalkShop();
        this._settings = new Gio.Settings({schema_id: 'com.feaneron.Spiel'});
    }

    vfunc_startup() {
        super.vfunc_startup();

        const styleManager = Adw.StyleManager.get_default();
        styleManager.colorScheme = Adw.ColorScheme.FORCE_DARK;

        this._talkShop.loadTalks();
    }

    vfunc_activate() {
        let {active_window} = this;

        if (!active_window)
            active_window = new SpielGreeter(this);

        active_window.present();
    }

    vfunc_shutdown() {
        for (const window of this.get_windows())
            window.close();

        super.vfunc_shutdown();
    }

    get talk_shop() {
        return this._talkShop;
    }

    get settings() {
        return this._settings;
    }
});
