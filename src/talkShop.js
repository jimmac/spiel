/* talkShop.js
 *
 * Copyright 2022 Georges Stavracas
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

import GLib from 'gi://GLib';
import Gio from 'gi://Gio';
import GObject from 'gi://GObject';

import {createSlide} from './slides.js';

export const SpielTalk = GObject.registerClass({
    GTypeName: 'SpielTalk',
    Properties: {
        'id': GObject.ParamSpec.string('id', '', '',
            GObject.ParamFlags.READWRITE | GObject.ParamFlags.CONSTRUCT_ONLY,
            ''),
        'name': GObject.ParamSpec.string('name', '', '',
            GObject.ParamFlags.READWRITE,
            ''),
        'creation-date': GObject.ParamSpec.uint64('creation-date', '', '',
            GObject.ParamFlags.READWRITE | GObject.ParamFlags.CONSTRUCT_ONLY,
            0, GLib.MAXUINT64_BIGINT, 0),
        'last-opened': GObject.ParamSpec.uint64('last-opened', '', '',
            GObject.ParamFlags.READWRITE,
            0, GLib.MAXUINT64_BIGINT, 0),
        'slide-deck': GObject.ParamSpec.object('slide-deck', '', '',
            GObject.ParamFlags.READWRITE,
            Gio.ListModel.$gtype),
    },
}, class SpielTalk extends GObject.Object {
});


export const SpielTalkShop = GObject.registerClass({
    GTypeName: 'SpielTalkShop',
    Properties: {
        'talks': GObject.ParamSpec.object('talks', '', '',
            GObject.ParamFlags.READABLE,
            Gio.ListModel.$gtype),
    },
}, class SpielTalkShop extends GObject.Object {
    constructor() {
        super();

        this._talks = new Gio.ListStore(SpielTalk.$gtype);

        // id -> SpielTalk
        this._talksMap = new Map();
    }

    _parseSlideDeck(slides) {
        const store = new Gio.ListStore(GObject.Object.$gtype);

        if (slides) {
            for (const slide of slides)
                store.append(createSlide(slide));
        }

        return store;
    }

    _slideDeckModelToJSON(model) {
        const deck = [];
        for (const slide of model)
            deck.push(slide.toJSON());
        return deck;
    }

    loadTalks() {
        const dir = Gio.File.new_for_path(GLib.get_user_data_dir());
        const children = dir.enumerate_children(
            'standard::name',
            Gio.FileQueryInfoFlags.NOFOLLOW_SYMLINKS,
            null);

        for (const fileInfo of children) {
            const filename = fileInfo.get_name();

            const parts = filename.split('.');

            if (parts.length != 2)
                continue
            if (parts[1] != 'talk')
                continue;
            if (!GLib.uuid_string_is_valid(parts[0]))
                continue;

            let properties;
            try {
                const file = dir.get_child(filename);
                const [success, contents] = GLib.file_get_contents(file.get_path());
                if (!success)
                    continue;

                const decoder = new TextDecoder();
                properties = JSON.parse(decoder.decode(contents));
                if (!properties)
                    continue;

                // Convert the slide deck into a GListModel
                properties['slide-deck'] = this._parseSlideDeck(properties['slide-deck']);
            } catch(e) {
                log(e.stack);
                continue;
            }

            const id = parts[0];
            const talk = new SpielTalk({id, ...properties});
            this._talks.append(talk);
            this._talksMap.set(id, talk);
        }
    }

    createTalk(name) {
        const now = GLib.DateTime.new_now_local();
        let id = GLib.uuid_string_random();

        while (this._talksMap.has(id))
            id = GLib.uuid_string_random();

        const dir = Gio.File.new_for_path(GLib.get_user_data_dir());
        const file = dir.get_child(`${id}.talk`);

        const properties = {
            name,
            'creation-date': now.to_unix(),
            'last-opened': now.to_unix(),
            'slide-deck': this._parseSlideDeck([
                {
                    type: 'opening',
                    title: name,
                    presenter: GLib.get_real_name(),
                    background: 'linear-gradient(200deg, @green_5 10%, @blue_5 90%)',
                },
                {
                    type: 'title',
                    title: _('Section 1'),
                    background: '@purple_5',
                },
                {
                    type: 'bullet-points',
                    title: _('Topics'),
                    bulletPoints: [
                        _('Topic 1'),
                        _('Topic 2'),
                        _('Topic 3'),
                    ],
                    background: '@purple_5',
                },
                {
                    type: 'title',
                    title: _('Section 2'),
                    background: '@green_5',
                },
                {
                    type: 'opening',
                    title: name,
                    presenter: GLib.get_real_name(),
                    background: 'linear-gradient(200deg, @green_5 10%, @blue_5 90%)',
                },
            ]),
        };

        try {
            GLib.file_set_contents(file.get_path(), JSON.stringify(properties, null, 2));
        } catch(e) {
            return null;
        }

        const talk = new SpielTalk({id, ...properties});
        this._talks.append(talk);
        this._talksMap.set(id, talk);

        return talk;
    }

    saveTalk(talk) {
        const now = GLib.DateTime.new_now_local();
        const dir = Gio.File.new_for_path(GLib.get_user_data_dir());
        const file = dir.get_child(`${talk.id}.talk`);

        const properties = {
            'name': talk.name,
            'creation-date': talk.creationDate,
            'last-opened': talk.lastOpened,
            'slide-deck': this._slideDeckModelToJSON(talk.slideDeck),
        };

        try {
            const contents = JSON.stringify(properties, null, 2);
            GLib.file_set_contents(file.get_path(), contents);
        } catch(e) {
        }
    }

    get talks() {
        return this._talks;
    }
});
