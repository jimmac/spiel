/* sentinelModel.js
 *
 * Copyright 2022 Georges Basile Stavracas Neto <georges.stavracas@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

import GLib from 'gi://GLib';
import Gio from 'gi://Gio';
import GObject from 'gi://GObject';

export const SpielSentinelItem = GObject.registerClass({
    GTypeName: 'SpielSentinelItem',
}, class SpielSentinelItem extends GObject.Object {
});

export const SpielSentinelListModel = GObject.registerClass({
    GTypeName: 'SpielSentinelListModel',
    Implements: [Gio.ListModel],
    Properties: {
        'model': GObject.ParamSpec.object('model', '', '',
            GObject.ParamFlags.READWRITE,
            Gio.ListModel.$gtype),
    },
}, class SpielSentinelListModel extends GObject.Object {
    constructor(params = {}) {
        super(params);

        this._sentinel = new SpielSentinelItem();
    }

    vfunc_get_item_type() {
        return GObject.Object.$gtype;
    }

    vfunc_get_n_items() {
        return (this._model ? this._model.get_n_items() : 0) + 1;
    }

    vfunc_get_item(index) {
        const nItems = this.get_n_items();

        if (index >= nItems)
            return null;

        return index === nItems - 1 ? this._sentinel : this._model?.get_item(index);
    }

    get model() {
        return this._model;
    }

    set model(v) {
        if (this._model === v)
            return;

        let nAdded = 0;
        let nRemoved = 0;

        if (this._model) {
            nRemoved = this._model.get_n_items();
            this._model.disconnect(this._itemsChangedId);
            delete this._itemsChangedId;
        }

        this._model = v;

        if (this._model) {
            nAdded = this._model.get_n_items();
            this._itemsChangedId = this._model.connect(
                'items-changed',
                (list, position, removed, added) => {
                    this.items_changed(position, removed, added);
                });
        }

        this.notify('model');
        this.items_changed(0, nRemoved, nAdded);
    }
});

