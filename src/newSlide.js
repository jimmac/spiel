/* newSlide.js
 *
 * Copyright 2022 Georges Basile Stavracas Neto <georges.stavracas@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

import Adw from 'gi://Adw';
import GLib from 'gi://GLib';
import Gio from 'gi://Gio';
import GObject from 'gi://GObject';
import Gtk from 'gi://Gtk';

import {listAvailableSlides} from './slides.js';

export const SpielNewSlideDialog = GObject.registerClass({
    GTypeName: 'SpielNewSlideDialog',
    Template: 'resource:///com/feaneron/Spiel/newSlide.ui',
    InternalChildren: ['slidesGroup'],
    Signals: {
        'create-slide': {
            flags: GObject.SignalFlags.RUN_FIRST,
            param_types: [GObject.TYPE_STRING],
        },
    }
}, class SpielNewSlideDialog extends Gtk.Dialog {
    constructor(params = {}) {
        super({
            ...params,
            use_header_bar: 1,
        });

        this._rows = new Map();
        this._activeRow = null;

        let group = null;
        for (const slide of listAvailableSlides()) {
            const radioButton = new Gtk.CheckButton({
                group,
                valign: Gtk.Align.CENTER,
            });
            radioButton.connect('notify::active', () => this._update());

            if (!group)
                group = radioButton;

            const row = new Adw.ActionRow({
                title: slide.name,
                subtitle: slide.description,
                activatable_widget: radioButton,
            });
            row.add_prefix(radioButton);
            this._slidesGroup.add(row);

            this._rows.set(row, {row, slide, radioButton});
        }

        this._update();
    }

    _update() {
        for (const data of this._rows.values()) {
            if (!data.radioButton.active)
                continue;

            this._activeRow = data.row;
            break;
        }

        this.set_response_sensitive(Gtk.ResponseType.OK, this._activeRow !== null);
    }

    vfunc_response(response) {
        if (response === Gtk.ResponseType.OK) {
            console.assert(this._activeRow !== null);
            const {slide} = this._rows.get(this._activeRow);
            this.emit('create-slide', slide.type);
        }
        this.close();
    }
});

